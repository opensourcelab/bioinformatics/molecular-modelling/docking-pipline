# Molecular Docking Pipeline

Molecular Docking Pipeline Framework.

## Features

## Installation

    pip install molecular_docking_pipeline --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    molecular_docking_pipeline --help 

## Development

    git clone gitlab.com/opensourcelab/docking-pipline

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/docking-pipline](https://opensourcelab.gitlab.io/docking-pipline) or [docking-pipline.gitlab.io](molecular_docking_pipeline.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



