#!/usr/bin/env python
"""Tests for `molecular_docking_pipeline` package."""
# pylint: disable=redefined-outer-name
from molecular_docking_pipeline import __version__
from molecular_docking_pipeline.molecular_docking_pipeline_interface import GreeterInterface
from molecular_docking_pipeline.molecular_docking_pipeline_impl import HelloWorld

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

