"""Top-level package for Molecular Docking Pipeline."""

__author__ = """mark doerr"""
__email__ = "mark.doerr@uni-greifswald.de"
__version__ = "0.0.1"
